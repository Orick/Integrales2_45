<?php

  $grado = $_POST['grado'];
  $n = $_POST['n'];
  $x = $_POST['x'];
  $inferior = $_POST['inferior'];
  $superior = $_POST['superior'];

  $h = ($superior - $inferior) / $n;

  function EvaluarFuncion($grado, $x, $e){
    $resultado = 0;
    for($i = 0; $i <= $grado; $i++){
      if($i == 0){
        $resultado += $x[$i];
      } else {
        $resultado += (pow($e, $i) * $x[$i]);
      }
    }
    return $resultado; // (Eso espero)
  }

  function GenerarTabla($inferior, $superior, $h, $grado, $x, $flag){
    for($i = $inferior; $i <= $superior; $i += $h){
      $row = "<tr><td>";
      $row .= $i;
      $row .= "</td>";
      $row .= "<td>";
      $row .= EvaluarFuncion($grado, $x, $i);
      $res['x'][] = $i;
      $res['y'][] = EvaluarFuncion($grado, $x, $i);
      $row .= "</td></tr>";
      if($flag){
        echo $row;
      }
    }
    return $res;
  }

  $res = GenerarTabla($inferior, $superior, $h, $grado, $x, false);

  function MultiplicarPatron($res){
    for($i = 0; $i < count($res['y']); $i++){
      if($i == 0 || $i == count($res['y']) - 1){
        $res['x'][$i] = 7;
        $res['y'][$i] *= 7;
      } else if($i % 2 != 0){
        $res['x'][$i] = 32;
        $res['y'][$i] *= 32;
      } else if($i % 2 == 0 && $i % 4 != 0){
        $res['x'][$i] = 12;
        $res['y'][$i] *= 12;
      } else if($i % 2 == 0 && $i % 4 == 0){
        $res['x'][$i] = 14;
        $res['y'][$i] *= 14;
      }
    }
    return $res;
  }

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Integral 2/45</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="master.css">
  </head>
  <body>
    <h1 class="display-3 text-center mt-3">Integral 2/45</h1>
    <p class="text-center mb-5">Métodos numéricos | Erick Santiago Amezcua Tarango</p>

    <div id="resultado" class="izquierda">
      <table class="table-striped">
        <tr>
          <th scope="col">X</th>
          <th scope="col">Y</th>
        </tr>
        <?php GenerarTabla($inferior, $superior, $h, $grado, $x, true); ?>
      </table>
      <table class="table-striped">
        <tr>
          <th scope="col">*</th>
          <th scope="col"> = </th>
        </tr>
        <?php
          $res = MultiplicarPatron($res);
          for ($i=0; $i < count($res['x']); $i++) {
            echo "<tr><td>";
            echo $res['x'][$i];
            echo "</td><td>";
            echo $res['y'][$i];
            echo "</td></tr>";
          }
        ?>
      </table>
    </div>

    <div class="float-right derecha">
      <h3>
      <?php
        $sum = array_sum($res['y']);
        $mul = $h * (2/45);

        $final = $sum * $mul;
        echo "La integral es: $final";
      ?>
      </h3>
    </div>

    <script src="js/jquery-3.2.1.min"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
