$(document).ready(function(){

  let $n = $("#n");
  let $grado = $("#grado");
  let validado = false;

  $n.keyup(function(){
    $n.removeClass("is-invalid");

    if($n.val() % 4 !== 0 && $n.val() > 0){
      //Cuando esta mal
      $n.removeClass("is-valid");
      $n.addClass("is-invalid");
      valido = false;
    } else {
      //Cuando esta bien
      $n.removeClass("is-invalid");
      $n.addClass("is-valid");
      valido = true;
    }
  });//Termina validación de n

  $(".paso-dos, button[type=submit]").hide();
  let formField;

  $("button[type=button]").click(function(){
    if($grado.val() > 0){
      $(".paso-dos input, .paso-dos label, br").remove();
      $(".paso-dos, button[type=submit]").show();
      for(let i = $grado.val() ; i >= 0; i--){
        if(i > 0){
          formField = "<label>x<sup>"+ i +"</sup>: </label>"
        }else{
          formField = "<label>CONST: </label>";
        }

        formField += "<input type='number' class='' value='0' name='x[" + i
        formField += "]' step='any' required><br>";

        $(".paso-dos").append(formField);
      }
    }
  });//Fin del button click

});// Fin del document ready
