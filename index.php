<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Integral 2/45</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="master.css">
  </head>
  <body>
    <h1 class="display-3 text-center mt-3">Integral 2/45</h1>
    <p class="text-center mb-5">Métodos numéricos | Erick Santiago Amezcua Tarango</p>
    <!-- Formulario-->
    <form action="calculo.php" method="post" class="contrainer col-4 centro" id="needs-validation" novalidate>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Datos</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Grado de la función:
            </td>
            <td>
              <input type="number" class="form-control" id="grado" value="0" name="grado" required>
              <div class="invalid-feedback">
                Este campo es obligatorio
              </div>
            </td>
          </tr>
          <tr>
          </tr>
          <tr>
            <td>
              N:
            </td>
            <td>
              <input type="number" class="form-control" id="n" value="0" name="n" step="any" required>
              <div class="invalid-feedback">
                Debe ser multiplo de 4 (preferentemente no 4).
              </div>
            </td>
          </tr>
          <tr>
            <th scope="col">Limite inferior</th>
            <th scope="col">Limite superior</th>
          </tr>
          <tr>
            <td>
              <input type="number" class="form-control" value="0" name="inferior" step="any" required>
            </td>
            <td>
              <input type="number" class="form-control" value="0" name="superior" step="any" required>
            </td>
          </tr>
        </tbody>
      </table>
      <button class="btn btn-primary" type="button">Siguiente paso</button>
      <div class="paso-dos mt-3">
      </div>
      <button class="btn btn-primary" type="submit">Calcular</button>
    </form>
      <!--<div class="row justify-content-center">
        <div class="col-md-1 mb-3">
          <label for="">X</label>
          <input type="number" class="form-control" id="x7" value="0" name="x7" step="any" required>
          <div class="invalid-feedback">
            Se requiere el valor de a
          </div>
        </div>
      </div>-->
    </form>
    <script src="js/jquery-3.2.1.min"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
